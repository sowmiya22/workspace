package bcas.pro.cal;
	/**
	 * To perform basic calculator functions
	 * 
	 * @author Sowmiya
	 * @since 17-05-2020
	 * @version 1.0.0v
	 *
	 */
public class SimpleCalculator {

	/**
	 * variable a, number 1 value
	 */
	public int a;

	/**
	 * variable b, number 2 value
	 */
	public int b;

	/**
	 * to add two integer numbers. Will return sum of given two integers(a+b)
	 * 
	 * @param a addition first value
	 * @param b addition second value
	 * @return (a+b)
	 */
	public int sum(int a, int b) {
		return a + b;
	}

	/**
	 * to subtract two integer numbers. Will return sum of given two integers(a-b /
	 * b-a)
	 * 
	 * @param a subtraction first value
	 * @param b subtraction second value
	 * @return (a-b)
	 */
	public int sub(int a, int b) {
		return a - b;
	}

	/**
	 * This is the main method of this program
	 * 
	 * @param args for input arguments
	 */
	public static void main(String[] args) {

		SimpleCalculator cal = new SimpleCalculator();
		System.out.println(cal.sub(10, 5));
	}
}
 
   