package string;

public class StringMethodPractice {
	

		
		public static void main(String[] args){
			
			String text = "Sowmiya";
			String text1 = "Satkunarasa";
			String text2 = ".com ";
			String text3 = "BeiginnersNovels/Books";
			String text4 = "   life is short   ";
			String text5 = "my name is sowmiya";


			System.out.println(" String charAt() Method ");
			System.out.println("input \t: " + text);
			System.out.println("output \t: " + text.charAt(3));
			System.out.println();


			System.out.println(" String concat() Method ");
			System.out.println("input \t: " + text3 + "," + text2);
			System.out.println("output \t: " + text2.concat(text3));
			System.out.println();


			System.out.println(" String indexOf(int ch, int fromIndex) Method ");
			System.out.println("input \t: " + text);
			System.out.println("output \t: " + text.indexOf("a", 3));
			System.out.println();


			System.out.println(" String indexOf(String str) Method ");
			System.out.println("input \t: " + text + "," + text1);
			System.out.println("output \t: " + text.indexOf(text1));
			System.out.println();


			System.out.println(" String indexOf(String str, int fromIndex) Method ");
			System.out.println("input \t: " + text2 + "," + text3);
			System.out.println("output \t: " + text2.indexOf(text3, 5));
			System.out.println();


			System.out.println(" String lastIndexOf() Method ");
			System.out.println("input \t: " + text);
			System.out.println("output \t: " + text.lastIndexOf("m"));
			System.out.println();


			System.out.println(" String lastIndexOf(int ch, int fromIndex) Method ");
			System.out.println("input \t: " + text);
			System.out.println("output \t: " + text.lastIndexOf("s", 4));
			System.out.println();


			System.out.println(" String lastIndexOf(String str) Method ");
			System.out.println("input \t: " + text2 + "," + text3);
			System.out.println("output \t: " + text2.lastIndexOf(text3));
			System.out.println();


			System.out.println(" String lastIndexOf() fromIndex Method ");
			System.out.println("input \t: " + text1 + "," + text);
			System.out.println("output \t: " + text1.lastIndexOf(text, 2));
			System.out.println();


			System.out.println(" String length() Method ");
			System.out.println("input \t: " + text1);
			System.out.println("output \t: " + text1.length());
			System.out.println();


			System.out.println(" String replace() Method ");
			System.out.println("input \t: " + text1);
			System.out.println("output \t: " + text1.replace("o", "i"));
			System.out.println();


			System.out.println(" String startsWith() Method ");
			System.out.println("input \t: " + text);
			System.out.println("output \t: " + text.startsWith("S"));
			System.out.println();


			System.out.println(" String substring() Method ");
			System.out.println("input \t: " + text);
			System.out.println("output \t: " + text.substring(4));
			System.out.println();


			System.out.println(" String substring(beginIndex, endIndex) Method ");
			System.out.println("input \t: " + text);
			System.out.println("output \t: " + text.substring(1, 4));
			System.out.println();


			System.out.println(" String toLowerCase() Method ");
			System.out.println("input \t: " + text5);
			System.out.println("output \t: " + text5.toLowerCase());
			System.out.println();


			System.out.println(" String toUpperCase() Method ");
			System.out.println("input \t: " + text1);
			System.out.println("output \t: " + text1.toUpperCase());
			System.out.println();


			System.out.println(" String trim() Method ");
			System.out.println("input \t: " + text4);
			System.out.println("output \t: " + text4.trim());
			System.out.println();
			
		}
	}



